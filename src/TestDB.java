import java.io.File;
import java.io.FileNotFoundException;


public class TestDB {
	public static void main(String[] args) 
			throws FileNotFoundException, MalformedEmailException {
		
		UWmailDB db = new UWmailDB();
		Email e0 = 
				ParseEmailTextFiles.parseOneMail(new File("./sampleZip/0.txt"));
		Email e2 = 
				ParseEmailTextFiles.parseOneMail(new File("./sampleZip/2.txt"));

		Email e3 = 
				ParseEmailTextFiles.parseOneMail(new File("./sampleZip/3.txt"));
		Email e4 = 
				ParseEmailTextFiles.parseOneMail(new File("./sampleZip/4.txt"));
		Email e5 = 
				ParseEmailTextFiles.parseOneMail(new File("./sampleZip/5.txt"));
		Email e6 = 
				ParseEmailTextFiles.parseOneMail(new File("./sampleZip/6.txt"));

		System.out.println(db.size());
		db.addEmail(e3);
		System.out.println(db.size());
		db.addEmail(e4);
		System.out.println(db.size());
		db.addEmail(e5);
		System.out.println(db.size());
		db.addEmail(e0);
		System.out.println(db.size());
		db.addEmail(e2);
		System.out.println(db.size());
		
		db.deleteConversation(1);
		System.out.println(db.size());
		/*
		System.out.println(db.getInbox());
		System.out.println(db.getTrash());
	*/
	}
}

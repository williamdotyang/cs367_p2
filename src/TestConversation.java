
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;

public class TestConversation {
	public static void main(String[] args) 
			throws FileNotFoundException, MalformedEmailException {
		Email e3 = 
				ParseEmailTextFiles.parseOneMail(new File("./sampleZip/3.txt"));
		Email e4 = 
				ParseEmailTextFiles.parseOneMail(new File("./sampleZip/4.txt"));
		Email e5 = 
				ParseEmailTextFiles.parseOneMail(new File("./sampleZip/5.txt"));
		Email e6 = 
				ParseEmailTextFiles.parseOneMail(new File("./sampleZip/6.txt"));
		
		System.out.println("date: " + e3.getDate());
		System.out.println("messageID: " + e3.getMessageID());
		System.out.println("subject: "+  e3.getSubject());
		System.out.println("from: " + e3.getFrom());
		System.out.println("to: " + e3.getTo());
		System.out.println("in-reply-to: " + e3.getInReplyTo());
		ListADT<String> references, body;
		references = e3.getReferences();
		body = e3.getBody();
		System.out.print("references: ");
		while (!references.isEmpty()) {
			System.out.println(references.remove(0));
		}
		System.out.print("body: ");
		while (!body.isEmpty()) {
			System.out.println(body.remove(0));
		}
		
		System.out.println("\nAdding threads...");
		Conversation thread = new Conversation(e3);
		thread.add(e4);
		thread.add(e5);
		thread.add(e6);
		System.out.println("thread size: " + thread.size());
		System.out.println("current index: " + thread.getCurrent());
		thread.moveCurrentForward();
		System.out.println("current index (last won't move forward): " + 
				thread.getCurrent());
		thread.moveCurrentBack();
		System.out.println("current index: " + thread.getCurrent());
		thread.moveCurrentBack();
		thread.moveCurrentBack();
		System.out.println("current index: " + thread.getCurrent());
		thread.moveCurrentBack();
		System.out.println("current index (first won't move back): " +
				thread.getCurrent());
		
		System.out.println("\nIterator for thread:");
		Iterator<Email> itr = thread.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next().getSubject());
		}
		
		System.out.println("\nTesting ParseEmailTextFile class...");
		File folder = new File("sampleZip");
		ListADT<Email> mails = ParseEmailTextFiles.parseMailsInFolder(folder);
		System.out.println("is null: " + (mails == null));
		for (Email mail : mails) {
			System.out.println(mail.getMessageID() + ": " + mail.getSubject());
		}
		
		System.out.println("\nzip reading...");
		mails = ParseEmailTextFiles.parseMailsInZip("sampleZip.zip");
		System.out.println("is null: " + (mails == null));
		for (Email mail : mails) {
			System.out.println(mail.getMessageID() + ": " + mail.getSubject());
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  UWmail.java
// File:             Email.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   fully acknowledge and credit all sources of help,
//                   other than Instructors and TAs.
//
// Persons:          Identify persons by name, relationship to you, and email.
//                   Describe in detail the the ideas and help they provided.
//
// Online sources:   avoid web searches to solve your problems, but if you do
//                   search, be sure to include Web URLs and description of 
//                   of any information you find.
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.Date;
import java.text.SimpleDateFormat;

public class Email {
	// private fields
	private Date date;
	private String messageID;
	private String subject;
	private String from;
	private String to;
	private ListADT<String> body;
	private String inReplyTo;
	private ListADT<String> references;

	// constructors
	/**
	 * Constructs an Email with the given attributes. 
	 * This is the constructor for an email that is the first in the 
	 * conversation, i.e. without In-Reply-To or References fields.
	 */
	public Email(Date date, String messageID, String subject, 
			String from, String to, ListADT<String> body) {
		
		if (date == null || messageID == null || subject == null || 
				from == null || to == null || body == null)
			throw new IllegalArgumentException("null in argument not allowed!");
		
		this.date = date;
		this.messageID = messageID;
		this.subject = subject;
		this.from = from;
		this.to = to;
		this.body = body;
		this.inReplyTo = null;
		this.references = null;
	}

	/**
	 * Constructs an Email with the given attributes. This is the constructor 
	 * for an email that is not the first in the conversation, i.e. contains 
	 * In-Reply-To and References fields.
	 */
	public Email(Date date, String messageID, String subject, 
			String from, String to, ListADT<String> body, 
			String inReplyTo, ListADT<String> references) {
		
		if (date == null || messageID == null || subject == null || 
				from == null || to == null || body == null || 
				inReplyTo == null || references == null)
			throw new IllegalArgumentException("null in argument not allowed!");
		
		this.date = date;
		this.messageID = messageID;
		this.subject = subject;
		this.from = from;
		this.to = to;
		this.body = body;
		this.inReplyTo = inReplyTo;
		this.references = references;
	}

	/**
	 * Return the date in a human-readable form. If the date on the email is 
	 * today, then you should format it with a SimpleDateFormat object and the 
	 * formatting string "h:mm a". Otherwise, format it with a SimpleDateFormat 
	 * object and the formatting string "MMM d".
	 * 
	 * @return A readable date string.
	 */
	public String getDate() {
		SimpleDateFormat dateWithoutTime = new SimpleDateFormat("yyyyMMdd");
		Date today = new Date();
		if (dateWithoutTime.format(date).equals(dateWithoutTime.format(today)))
			return (new SimpleDateFormat("h:mm a")).format(date);
		else
			return (new SimpleDateFormat("MMM d")).format(date);
	}

	/**
	 * Return the unique email identifier that was stored in the 
	 * Message-ID field.
	 * 
	 * @return Message-ID
	 */
	public String getMessageID() {
		return messageID;
	}

	/**
	 * Return what was stored in the Subject: field.
	 * 
	 * @return subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Return what was stored in the From: field.
	 * 
	 * @return from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * Return what was stored in the To: field.
	 * 
	 * @return to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * Return the lines of text from the end of the header to the end 
	 * of the file.
	 * 
	 * @return List object storing the body of the email.
	 */
	public ListADT<String> getBody() {
		return body;
	}

	/**
	 * Return what was stored in the In-Reply-To: field. 
	 * If the email was the first in the conversation, return null.
	 * 
	 * @return In-Reply-To, or null.
	 */
	public String getInReplyTo() {
		return inReplyTo;
	}

	/**
	 * Return the Message-ID's from the References: field. 
	 * If the email was the first in the conversation, return null.
	 * 
	 * @return references, or null.
	 */
	public ListADT<String> getReferences() {
		return references;
	}
} 

///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  UWmail.java
// File:             DoublyLinkedListIterator.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   fully acknowledge and credit all sources of help,
//                   other than Instructors and TAs.
//
// Persons:          Identify persons by name, relationship to you, and email.
//                   Describe in detail the the ideas and help they provided.
//
// Online sources:   avoid web searches to solve your problems, but if you do
//                   search, be sure to include Web URLs and description of 
//                   of any information you find.
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * Iterator class for DoublyLinkedList class.
 *
 * <p>Bugs: (a list of bugs and other problems)
 *
 * @author Weilan Yang
 */
public class DoublyLinkedListIterator<E> implements Iterator<E> {

	// fields
	private Listnode<E> head;
	private Listnode<E> curr;
	
	// constructor
	public DoublyLinkedListIterator(Listnode<E> head) {
		curr = head.getNext();
		this.head = head;
	}
	
	/**
	 * Test if there's any more items in the list.
	 * 
	 * @return true if there is, false otherwise.
	 */
	@Override
	public boolean hasNext() {
		return curr != head;
	}

	/**
	 * Get the data in current position and move the pointer one step after.
	 * 
	 * @return Data field stored in current position.
	 */
	@Override
	public E next() {
		if (! hasNext())
			throw new NoSuchElementException();
		
		E result = curr.getData();
		curr = curr.getNext();
		return result;
	}
	
	/**
	 * Not supported method.
	 */
	public void remove() {
        throw new UnsupportedOperationException();
    }
	
}
///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  UWmail.java
// File:             DoublyLinkedList.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   fully acknowledge and credit all sources of help,
//                   other than Instructors and TAs.
//
// Persons:          Identify persons by name, relationship to you, and email.
//                   Describe in detail the the ideas and help they provided.
//
// Online sources:   avoid web searches to solve your problems, but if you do
//                   search, be sure to include Web URLs and description of 
//                   of any information you find.
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.Iterator;


/**
 * An implementation of ListADT using doubly linked list, with circular 
 * reference. Besides, it has an empty header node.
 *
 * <p>Bugs: (a list of bugs and other problems)
 *
 * @author Weilan Yang
 */
public class DoublyLinkedList<E> implements ListADT<E>, Iterable<E> {
	
	// fields
	private int numItems;
	private Listnode<E> head;
	
	// constructors
//	public DoublyLinkedList(E item) {
//		if (item == null) 
//			throw new IllegalArgumentException("Item cannot be null!");
//
//		// header node
//		head = new Listnode<E>(null, null, null);
//		Listnode<E> firstNode = new Listnode<E>(item, head, head);
//		head.setNext(firstNode);
//		head.setPrev(firstNode);
//		numItems = 1;
//	}
	
	public DoublyLinkedList() {
		// just a header node
		head = new Listnode<E>(null, null, null);
		head.setNext(head);
		head.setPrev(head);
		numItems = 0;
	}
	
	/**
	 * Create an iterator object.
	 * 
	 * @return An iterator object.
	 */
	@Override
	public Iterator<E> iterator() {
		return new DoublyLinkedListIterator<E>(head);
	}

	
	/**
	 * Add a new node at the end.
	 *
	 * @param item The data field in the new node.
	 */
	@Override
	public void add(E item) {
		if (item == null) 
			throw new IllegalArgumentException("Item cannot be null");
		
		Listnode<E> newNode = new Listnode<E>(item, head, head.getPrev());
		head.getPrev().setNext(newNode);
		head.setPrev(newNode);
		numItems++;		
	}

	/**
	 * Add a new node at pos (zero-based index).
	 * 
	 * @param pos Position to add the new node (zero-based).
	 * @param item The data field in the new node.
	 */
	@Override
	public void add(int pos, E item) {
		if (item == null)
			throw new IllegalArgumentException("Item cannot be null!");
		if (pos < 0 || pos > numItems)
			throw new IndexOutOfBoundsException("Invalid position!");
		
		if (pos == numItems) {
			add(item);
		} else {
			Listnode<E> curr = head;
			for (int i = 0; i < pos; i++)
				curr = curr.getNext();
			
			Listnode<E> newNode = new Listnode<E>(item, curr.getNext(), curr);
			curr.getNext().setPrev(newNode);
			curr.setNext(newNode);
			numItems++;
		}
	}

	/**
	 * Test if a given item is in the list, i.e. if there's a node containing 
	 * this item as a data field.
	 * 
	 * @param item The data to be searched in the list.
	 * @return true if it contains, false otherwise.
	 */
	@Override
	public boolean contains(E item) {
		Iterator<E> itr = iterator();
		while(itr.hasNext()) {
			if (item.equals(itr.next())) 
				return true;
		}
		return false;
	}

	/**
	 * Getter method for accessing the data field stored in (pos+1)th node in 
	 * the list.
	 * 
	 *  @param pos Index of the node (zero-based).
	 *  @return The data field.
	 */
	@Override
	public E get(int pos) {
		if (pos < 0 || pos >= numItems)
			throw new IndexOutOfBoundsException("Bad pos.");
		
		Iterator<E> itr = iterator();
		for (int i = 0; i < pos; i++)
			itr.next();
		
		return itr.next();
	}

	/**
	 * Test if this list is empty.
	 * 
	 * @return true if it's empty, false otherwise.
	 */
	@Override
	public boolean isEmpty() {
		return numItems == 0;
	}

	/**
	 * Remove a node at pos, and then return the data field stored in the 
	 * removed node.
	 * 
	 * @param pos Index of the node to be removed.
	 * @return Data field stored in the removed node.
	 */
	@Override
	public E remove(int pos) {
		if (pos < 0 || pos >= numItems)
			throw new IndexOutOfBoundsException("Bad pos.");
		
		Listnode<E> curr = head;
		for (int i = 0; i < pos; i++) 
			curr = curr.getNext();
		
		E result = curr.getNext().getData();
		curr.setNext(curr.getNext().getNext());
		curr.getNext().setPrev(curr);
		numItems--;
		
		return result;
	}

	/**
	 * Get the number of items in the list.
	 * 
	 * @return The field numItems.
	 */
	@Override
	public int size() {
		return numItems;
	}
	
}
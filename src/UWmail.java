///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Title:            p2
// Files:            Conversation.java, DoublyLinkedList.java, 
//	DoublyLinkedListIterator.java, Email.java, ListADT.java, Listnode.java, 
//	MalformedEmailException.java, ParseEmailTextFiles.java, UWmail.java, 
//	UWmailDB.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang
// Email:            wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   must fully acknowledge and credit those sources of help.
//                   Instructors and TAs do not have to be credited here,
//                   but tutors, roommates, relatives, strangers, etc do.
//
// Persons:          Identify persons by name, relationship to you, and email.
//                   Describe in detail the the ideas and help they provided.
//
// Online sources:   avoid web searches to solve your problems, but if you do
//                   search, be sure to include Web URLs and description of 
//                   of any information you find.
//////////////////////////// 80 columns wide //////////////////////////////////

import java.util.Scanner;
import java.lang.Integer;
import java.lang.NumberFormatException;
import java.util.Iterator;

public class UWmail {
	private static UWmailDB uwmailDB = new UWmailDB();
	private static final Scanner stdin = new Scanner(System.in);

	public static void main(String args[]) {
		if (args.length != 1) {
			System.out.println("Usage: java UWmail [EMAIL_ZIP_FILE]");
			System.exit(0);
		}

		loadEmails(args[0]);

		displayInbox();
	}

	private static void loadEmails(String fileName) {
		ListADT<Email> mails;
		mails = ParseEmailTextFiles.parseMailsInZip(fileName);
		for (Email mail : mails) 
			uwmailDB.addEmail(mail);
	}

	private static void displayInbox(){
		boolean done = false;

		ListADT<Conversation> inbox = uwmailDB.getInbox();

		if (inbox.isEmpty())
			System.out.println("No conversations to show.");

		System.out.println("Inbox:");
		System.out.println("---------------------------------------------------"
				+ "-----------------------------");

		Iterator<Conversation> itr = inbox.iterator();    
		int i = 0;
		while (itr.hasNext())	{
			Conversation temp = itr.next();
			System.out.println("[" + i++ + "] " + temp.get(0).getSubject() + 
					" " + "(" + temp.get(temp.size() - 1).getDate() + ")");
		}


		while (!done) 
		{
			System.out.print("Enter option ([#]Open conversation, [T]rash, " + 
					"[Q]uit): ");
			String input = stdin.nextLine();

			if (input.length() > 0) 
			{

				int val = 0;
				boolean isNum = true;

				try {
					val = Integer.parseInt(input);
				} catch (NumberFormatException e) {
					isNum = false;
				}

				if(isNum) {
					if(val < 0) {
						System.out.println("The value can't be negative!");
						continue;
					} else if (val >= uwmailDB.size()) {
						System.out.println("Not a valid number!");
						continue;
					} else {
						displayConversation(val);
						continue;
					}

				}

				if(input.length()>1)
				{
					System.out.println("Invalid command!");
					continue;
				}

				switch(input.charAt(0)){
				case 'T':
				case 't':
					displayTrash();
					break;

				case 'Q':
				case 'q':
					System.out.println("Quitting...");
					done = true;
					break;

				default:  
					System.out.println("Invalid command!");
					break;
				}
			} 
		} 
		System.exit(0);
	}

	private static void displayTrash(){

		boolean done = false;

		ListADT<Conversation> trash = uwmailDB.getTrash();

		System.out.println("Trash:");
		System.out.println("---------------------------------------------------"
				+ "-----------------------------");

		if (trash.isEmpty())	{
			System.out.println("No conversations to show.");
		} else {

			Iterator<Conversation> itr = trash.iterator();    
			int i = 0;
			while (itr.hasNext())	{
				Conversation temp = itr.next();
				System.out.println("[" + i++ + "] " + 
						temp.get(temp.size() - 1).getSubject() + " " + 
						"(" + temp.get(temp.size() - 1).getDate() + ")");
			}
		}

		while (!done) 
		{
			System.out.print("Enter option ([I]nbox, [Q]uit): ");
			String input = stdin.nextLine();

			if (input.length() > 0) 
			{
				if(input.length()>1)
				{
					System.out.println("Invalid command!");
					continue;
				}

				switch(input.charAt(0)){
				case 'I':
				case 'i':
					displayInbox();
					break;

				case 'Q':
				case 'q':
					System.out.println("Quitting...");
					done = true;
					break;

				default:  
					System.out.println("Invalid command!");
					break;
				}
			} 
		} 
		System.exit(0);
	}

	private static void displayConversation(int val) {

		if (val >= uwmailDB.size() || val < 0 )		return;

		boolean done = false;

		ListADT<Conversation> inbox = uwmailDB.getInbox();
		Conversation conv = inbox.get(val);
		Iterator<Email> itr = conv.iterator();
		//Email temp  = itr.next();

		int curr = conv.getCurrent();
		Email currEmail = conv.get(curr);

		if (curr == 0)	{
			System.out.println("SUBJECT: " + conv.get(0).getSubject());
			System.out.println("-----------------------------------------------"
					+ "---------------------------------") ;		
			System.out.println("From: " + currEmail.getFrom());
			System.out.println("To: " + currEmail.getTo());
			System.out.println(currEmail.getDate());
			System.out.println();
			for (int j = 0; j < currEmail.getBody().size(); j++)
				System.out.println(currEmail.getBody().get(j));
			System.out.println("-----------------------------------------------"
					+ "---------------------------------") ;
			itr.next();
		}

		else {
			System.out.println("SUBJECT: " + conv.get(0).getSubject());
			System.out.println("-----------------------------------------------"
					+ "---------------------------------") ;		
			for (int i = 0; i < curr; i ++ )	{
				Email temp  = itr.next();
				System.out.print(temp.getFrom() + " | ");
				System.out.print(temp.getBody().get(0) + " | ");
				System.out.println(temp.getDate());
				System.out.println("-------------------------------------------"
						+ "-------------------------------------") ;
			}
			itr.next();

			System.out.println("From: " + currEmail.getFrom());
			System.out.println("To: " + currEmail.getTo());
			System.out.println(currEmail.getDate());
			System.out.println();
			for (int j = 0; j < currEmail.getBody().size(); j++)
				System.out.println(currEmail.getBody().get(j));
			System.out.println("-----------------------------------------------"
					+ "---------------------------------") ;

		}

		while (itr.hasNext())	{
			Email temp  = itr.next();
			System.out.print(temp.getFrom() + " | ");
			System.out.print(temp.getBody().get(0) + " | ");
			System.out.println(temp.getDate());
			System.out.println("-----------------------------------------------"
					+ "---------------------------------") ;
		}

		while (!done) 
		{
			System.out.print("Enter option ([N]ext email, [P]revious email, " +
				"[J]Next conversation, [K]Previous\nconversation, [I]nbox, " +
				"[#]Move to trash, [Q]uit): ");
			String input = stdin.nextLine();

			if (input.length() > 0) 
			{

				if(input.length()>1)
				{
					System.out.println("Invalid command!");
					continue;
				}

				switch(input.charAt(0)){
				case 'P':
				case 'p':
					conv.moveCurrentBack();
					displayConversation(val);
					break;
				case 'N':
				case 'n':
					conv.moveCurrentForward();
					displayConversation(val);
					break;
				case 'J':
				case 'j':
					if (val == inbox.size() - 1)
						displayInbox();
					else 
						displayConversation(++val);
					break;

				case 'K':
				case 'k':
					if (val == 0)
						displayInbox();
					else 
						displayConversation(--val);
					break;

				case 'I':
				case 'i':
					displayInbox();
					return;

				case 'Q':
				case 'q':
					System.out.println("Quitting...");
					done = true;
					break;

				case '#':
				// add delete conversation functionality. This conversation
				//should be moved to the trash when # is entered, and you should
				//take the user back to the inbox and continue processing input.
				//

					uwmailDB.deleteConversation(val);

					displayInbox();

					return;

				default:  
					System.out.println("Invalid command!");
					break;
				}
			} 
		} 
		System.exit(0);
	}
}
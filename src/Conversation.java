///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  UWmail.java
// File:             Conversation.java
// Semester:         CS367 Fall 2015
//
// Author:           Huilin Hu, hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Weilan Yang
// Email:            wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   fully acknowledge and credit all sources of help,
//                   other than Instructors and TAs.
//
// Persons:          Identify persons by name, relationship to you, and email.
//                   Describe in detail the the ideas and help they provided.
//
// Online sources:   avoid web searches to solve your problems, but if you do
//                   search, be sure to include Web URLs and description of 
//                   of any information you find.
//////////////////////////// 80 columns wide //////////////////////////////////

import java.util.Iterator;

/**
 * Creating a Conversation class whose internal structure is a DoublyLinkedList.
 * Also implementing the Iterable class. 
 * 
 *
 * <p>Bugs: none found
 *
 * @author Huilin Hu
 */

public class Conversation implements Iterable<Email> {
	/** A DoublyLinkedList object called list storing a list of emails */
	private DoublyLinkedList<Email> list;
	
	/** An integer object called current*/
	private Integer current;
	
	/**
	 * Constructs a conversation class object with an email object as the argument
	 * 
	 * @param e an object of Email class
	 * 
	 */
	public Conversation(Email e) {
		list = new DoublyLinkedList<Email> ();
		list.add(e);
		current = null;
	}

	/**
	 * returns index of the most recent email 
	 * 
	 * @param (none)
	 */
	
	public int getCurrent() {
		if (current == null) 
			current = size() - 1;
		  
		return current;
	 
	}
	
	/**
	 * Reduces current value by 1, which means move the pointer to the last 
	 * viewed email back one,
	 * 
	 */
	public void moveCurrentBack() {
	  
		if (this.getCurrent() > 0)
			current --; 
	}

	/**
	 * Increases current value by 1, which means move the pointer to the 
	 * last viewed email forward one
	 * 
	 */
	public void moveCurrentForward() {
		if (current < this.size() - 1)
			current ++ ;
	}	

	/**
	 * Returns the number of emails in a conversation list
	 * 
	 * @return the length of the conversation list
	 */
	public int size() {
		return list.size();
	}
	
	/**
	 * Returns the email object indexed by n 
	 * 
	 * @param n index of email in the list
	 * @return the email object indexed by n 
	 */
	public Email get(int n) {
		Email currEmail = list.get(n);
		return currEmail;
	}
	
	/**
	 * 
	 * Adds the email to the beginning of the conversation
	 * @param e an Email object
	 */
	public void add(Email e) {
		list.add(0, e);
	}

	/**
	 * Returns an iterator for conversation
	 * 
	 */
	public Iterator<Email> iterator() {
		return list.iterator();
	}

}

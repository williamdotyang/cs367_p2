
import java.util.Iterator;

public class TestDoublyLinkedList {
	public static void main(String[] args) {
		// test DoublyLinkedList
		DoublyLinkedList<Integer> list = new DoublyLinkedList<Integer>();
		System.out.println(list.isEmpty());
		System.out.println(list.size());
		
		list.add(1);
		System.out.println(list.isEmpty());
		System.out.println(list.size());
		
		list.add(2);
		System.out.println(list.isEmpty());
		System.out.println(list.size());
		
		list.add(2, 1); // "pos" can be invalid
		System.out.println(list.isEmpty());
		System.out.println(list.size());
	
		System.out.println(list.contains(4));
		//System.out.println(list.size());
		
		
		//list.get(5);
		System.out.println(list.get(0)); // "pos" can be invalid
		/*
		 * get(0) returns null. I suspects that it's illegal 
		 */
		System.out.println(list.get(1)); // "pos" can be invalid
		System.out.println(list.get(2)); // "pos" can be invalid
		//System.out.println(list.get(3)); // "pos" can be invalid
		System.out.println(list.size());
		
		list.remove(2);
		
		System.out.println(list.get(2)); // "pos" can be invalid
		/* 
		 * get(2) should be invalid but it returns 1 
		 * 
		 */
		
		//list.remove(3);
		
		/*
		// test DoublyLinkedListIterator
		Iterator<Integer> itr = list.iterator();
		itr.hasNext();
		itr.next();
		itr.remove();
		*/
			
		
	}
}


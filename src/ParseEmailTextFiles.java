///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  UWmail.java
// File:             ParseEmailTextFiles.java
// Semester:         CS367 Fall 2015
//
// Author:           Weilan Yang, wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Huilin Hu
// Email:            hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   fully acknowledge and credit all sources of help,
//                   other than Instructors and TAs.
//
// Persons:          Identify persons by name, relationship to you, and email.
//                   Describe in detail the the ideas and help they provided.
//
// Online sources:   avoid web searches to solve your problems, but if you do
//                   search, be sure to include Web URLs and description of 
//                   of any information you find.
//////////////////////////// 80 columns wide //////////////////////////////////

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.Enumeration;
import java.io.InputStream;
import java.io.IOException;
import java.util.zip.ZipException;

/**
 * This is a helper class used to parse the files in Zip folder.
 *
 * <p>Bugs: (a list of bugs and other problems)
 *
 * @author Weilan Yang
 */
public class ParseEmailTextFiles {
	// private fields
	private static final SimpleDateFormat FULL_DATE = 
			new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");

	/**
	 * Looks into a .zip file and parses all the .txt files it contains to 
	 * Email objects, and returns a list of Email objects.
	 * 
	 * @param zipFilename A string of zipped file's name.
	 * @return A list of Email objects.
	 * @throws MalformedEmailException 
	 */
	public static ListADT<Email> parseMailsInZip(String zipFilename) {
		ListADT<Email> mails = new DoublyLinkedList<Email>();
		
		try (ZipFile zf = new ZipFile(zipFilename);) {
			//follow this approach for using <? extends ZipEntry>, even though we will not cover this in class.
			Enumeration<? extends ZipEntry> entries = zf.entries();
			
			// for each file in the zipped folder
			while(entries.hasMoreElements()) {
				ZipEntry ze = entries.nextElement();
				if(ze.getName().endsWith(".txt")) {
					InputStream in = zf.getInputStream(ze);
					Scanner sc = new Scanner(in);
					// tries to create an Email object
					try {
						Email mail = parseOneMailFromStream(sc);
						mails.add(mail);
					} catch (MalformedEmailException e) {
						// DO NOTHING, JUST SKIP
						//System.err.println(zipFilename + 
						//		" contains malformed email files");
					} finally {
						sc.close();
					}
				}//end of iteration through .txt files
			}//end of iteration through all files in .zip file
			
		} catch (ZipException e) {
			System.err.println("File " + zipFilename + " not found.");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("An I/O error has occurred for the file.");
			System.exit(1);
		} catch (SecurityException e) {
			System.err.println("Unable to obtain read access for the file.");
			System.exit(1);
		} 
		return mails;
	}
	
	/**
	 * Given a Scanner to a file, parse it correctly into an Email object.
	 * 
	 * @param fileIn A Scanner object to a file.
	 * @return An Email object.
	 * @throws MalformedEmailException
	 */
	public static Email parseOneMailFromStream(Scanner fileIn) 
			throws MalformedEmailException {

		// get header of email, at most 7 lines
		int numLine = 1;
		String inReplyTo = null, messageID = null, subject = null, 
				from = null, to = null;
		ListADT<String> references = new DoublyLinkedList<String>();
		ListADT<String> body = new DoublyLinkedList<String>();
		Date date = null;

		try {
			// parse the headers
			while (fileIn.hasNextLine() && !(numLine > 7)) {
				String line = fileIn.nextLine();
				String[] pair = line.split(": ", 2);
				if (pair.length != 2) 
					throw new ParseException(line, numLine);

				String key = pair[0];
				String value = pair[1];
				
				if (value.equals("")) throw new ParseException(line, numLine);
				
				if (key.equals("In-Reply-To")) {
					inReplyTo = value;
				} else if (key.equals("References")) {
					String[] refStrs = value.split(", ");
					for (String refString : refStrs) 
						references.add(refString);
				} else if (key.equals("Date")) {
					date = FULL_DATE.parse(value);
				} else if (key.equals("Message-ID")) {
					messageID = value;
				} else if (key.equals("Subject")) {
					subject = value;
				} else if (key.equals("From")) {
					from = value;
				} else if (key.equals("To")) {
					to = value;
					break;
				}
				numLine++;
			}

			// parse the body
			while (fileIn.hasNextLine()) {
				body.add(fileIn.nextLine());
			}
		} catch (ParseException e) {	
			throw new MalformedEmailException("value missing");
		} finally {
			fileIn.close();
		}

		// create an Email object
		Email email = null;
		try {
			if (inReplyTo == null && references.isEmpty()) {
				email = new Email(date, messageID, subject, from, to, body);
			} else {
				email = new Email(date, messageID, subject, from, to, body, 
						inReplyTo, references);
			}
		} catch (IllegalArgumentException e) {
			throw new MalformedEmailException("key missing");
		}
		
		return email;
	}
	
	/**
	 * Looks into a directory and parse all the files into Email objects and 
	 * return them as a list. If a FileNotFoundException is raised, displays 
	 * error message and return null; If a file is malformed, throws a
	 * MalformedEmailException.
	 * 
	 * @param folder A directory where the email text files resides.
	 * @return A list of Email, or null if there's exceptions.
	 * @throws MalformedEmailException
	 */
	public static ListADT<Email> parseMailsInFolder(File folder) 
			throws MalformedEmailException{
		DoublyLinkedList<Email> allMails = new DoublyLinkedList<Email>();
		String[] filenames = folder.list();

		for (String filename : filenames) {
			filename = folder + "/" + filename;
			//System.out.println("Parsing " + filename + "...");
			try {
				Email mail = parseOneMail(new File(filename));
				allMails.add(mail);
			} catch (FileNotFoundException e1) {
				System.err.println(filename + " not found.");
				return null;
			} catch (MalformedEmailException e2) {
				throw e2;
			}
		}
		return allMails;
	}

	/**
	 * Looks into a file and parse it correctly into an Email object.
	 * 
	 * @param filename A File object.
	 * @return An Email object.
	 * @throws FileNotFoundException
	 * @throws MalformedEmailException
	 */
	public static Email parseOneMail(File file) 
			throws FileNotFoundException, MalformedEmailException {
		Scanner fileIn = new Scanner(file);
		Email email = parseOneMailFromStream(fileIn);
		return email;
	}

}

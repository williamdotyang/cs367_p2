///////////////////////////////////////////////////////////////////////////////
//                   ALL STUDENTS COMPLETE THESE SECTIONS
// Main Class File:  UWmail.java
// File:             UWmailDB.java
// Semester:         CS367 Fall 2015
//
// Author:           Huilin Hu, hhu28@wisc.edu
// CS Login:         huilin
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// PAIR PROGRAMMERS COMPLETE THIS SECTION ////////////////////
//
// Pair Partner:     Weilan Yang
// Email:            wyang65@wisc.edu
// CS Login:         wyang
// Lecturer's Name:  Jim Skrentny
// Lab Section:      
//
//////////////////// STUDENTS WHO GET HELP FROM OTHER THAN THEIR PARTNER //////
//                   fully acknowledge and credit all sources of help,
//                   other than Instructors and TAs.
//
// Persons:          Identify persons by name, relationship to you, and email.
//                   Describe in detail the the ideas and help they provided.
//
// Online sources:   avoid web searches to solve your problems, but if you do
//                   search, be sure to include Web URLs and description of 
//                   of any information you find.
//////////////////////////// 80 columns wide //////////////////////////////////

/**
 * Creating a database class which represents our UWmail account, 
 * consisting of the inbox and the trash
 *
 * <p>Bugs: none found
 *
 * @author Huilin Hu
 */

public class UWmailDB {
	/** A DoublyLinkedList created as an inbox storing conversations */
	private DoublyLinkedList<Conversation> inbox; 
	
	/** A DoublyLinkedList created as a trash bin storing deleted conversations */
	private DoublyLinkedList<Conversation> trash;
	
	/** An counter recording the number of conversations in the inbox*/
	private int numItems; 
	
	/**
	 * Constructs a database object with without argument
	 * 
	 * @param none
	 * 
	 */
	public UWmailDB() {
		inbox = new DoublyLinkedList<Conversation>();
		trash = new DoublyLinkedList<Conversation>();
		numItems = 0 ;
	}

	/**
	 * Returns the number of conversations in the database
	 * @return number of conversations in database
	 */
	public int size() {
		return numItems;
	}

	/**
	 * Determines the correct email conversation to add a new email to, and does so. 
	 * If there is no such conversation, adds a new one to the inbox.
	 * @param e a valid email object
	 */
	public void addEmail(Email e) {
		
		boolean found = false;
		
		for (int i = 0; i < size(); i++)	{
			Conversation c = inbox.get(i);
			Email top = c.get(0);
			ListADT<String> refs = top.getReferences();
			if (refs == null) continue;
			if (top.getReferences().contains(e.getMessageID()))	{
				inbox.get(i).add(e);
				found = true;
				break;
			}
		}
		
		if (!found)	{
			inbox.add(new Conversation (e));
			numItems ++; 
		}
		
	}

	/**
	 * Returns the list of conversations in the inbox
	 * @return list of conversations in the inbox
	 */
	public ListADT<Conversation> getInbox() {
		return inbox;
	}

	/**
	 * Returns the list of conversations in the trash
	 * @return list of conversations in the trash
	 */
	public ListADT<Conversation> getTrash() {
		return trash;
	}

	/**
	 * Moves the conversation at the nth index of the inbox to the trash.
	 * @param idx
	 */
	public void deleteConversation(int idx) {
		Conversation deletedConver = inbox.remove(idx);
		trash.add(deletedConver);
		numItems ++;
	}

}
